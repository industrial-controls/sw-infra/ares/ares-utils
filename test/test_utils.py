from ares.utils import *
import unittest
import json
import yaml
import logging

class TestMongoDbCleanup(unittest.TestCase):
    def setUp(self) -> None:
        self.components_list = []
        with open("test/components.yaml","r") as f:
            self.components_list = yaml.safe_load(f)
    
    def test_simple_cleanup(self):
        mongodb_data : dict = None
        cleaned_up_data = None
        with open("test/mongodb.jcop.json","r") as f:
            mongodb_data = json.load(f)
            cleaned_up_data = cleanup_mongodb_data(mongodb_data)

        assert len(cleaned_up_data) > 0
        assert cleaned_up_data[0]["_id"] is not None
        assert mongodb_data["rows"][0] is not None
        assert mongodb_data["rows"][0]["_id"].get("$oid") == cleaned_up_data[0]["_id"]
    
    def test_enrich_components_list(self):
        releases = []
        with open("test/mongodb.jcop.json","r") as f:
            releases = cleanup_mongodb_data(json.load(f))
        
        assert len(releases) > 0
        assert len(self.components_list) > 0
        enrich_components_list(self.components_list, releases, "JCOP Framework ")
        assert self.components_list[0]["Latest"] is not None
        assert self.components_list[0]["Latest"] == "1.2.5"
        assert self.components_list[0]["zipDownloadUrl"] == "https://edms.cern.ch/file/1058671/1.2.5/fw3DViewer-1.2.5.zip"
        # Test that the pre-set URL does not get overwritten :
        assert self.components_list[1]["Latest"] is not None
        assert self.components_list[1]["URL"] is not None
        assert self.components_list[1]["URL"] == "already-specified.html"

        
    def test_generate_component_versions_files(self):
        releases = []
        with open("test/mongodb.jcop.json","r") as f:
            releases = cleanup_mongodb_data(json.load(f))
        releases_dict = generate_component_versions_files(self.components_list, releases, "JCOP Framework ")
        assert "3d-viewer" in releases_dict

    def test_generate_categories_versions_files(self):
        releases = []
        with open("test/mongodb.jcop.json","r") as f:
            releases = cleanup_mongodb_data(json.load(f))
        enrich_components_list(self.components_list, releases, "JCOP Framework ")
        # Test the list was enriched
        assert self.components_list[1]["Latest"] is not None
        cat_to_releases = generate_categories_versions_files(self.components_list, releases, "JCOP Framework ")
        assert len(cat_to_releases) > 0
        # All releases belong to category Tools
        assert len(cat_to_releases["Tools"]) == len(releases)
        # The order should not have changed overall and all items should be there
        for i,item in enumerate(cat_to_releases["Tools"]):
          assert cat_to_releases["Tools"][i] == releases[i]


    def test_denormalize_property(self):
        releases = []
        with open("test/mongodb.jcop.json","r") as f:
            releases = cleanup_mongodb_data(json.load(f))
        
        denormalize_property(releases,"platforms", "OK", "Not OK")

        assert "platforms" in releases[0]
        assert "platforms_pvss-3-8-sp2" in releases[0]
        assert releases[0]["platforms_pvss-3-8-sp2"] == "OK"
        assert releases[0]["platforms_pvss-3-8-sp1"] == "Not OK"
        assert releases[1]["platforms_pvss-3-8-sp1"] == "OK"
        assert releases[8]["platforms_wincc-oa-3-14"] == "OK"


if __name__ == '__main__':
    unittest.main()
