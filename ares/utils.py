from typing import List, Dict
import os
import logging
import traceback

from copy import deepcopy
import yaml

from pkg_resources import parse_version

"""
Clean up MongoDB data, remove problematic characters (JQuery pre-processing)
"""
def cleanup_mongodb_data(mongodb_data) -> list:
    data_copy = deepcopy(mongodb_data)
    result = []
    if(type(data_copy) is dict and "rows" in data_copy):
        result = deepcopy(data_copy["rows"])
    else:
        result = deepcopy(data_copy)

    # Collapse all _id.$oid into _id
    for row in result:
        if (type(row) is dict) and "_id" in row and "$oid" in row["_id"]:
            row["_id"] = row["_id"]["$oid"]

    return result

"""
Given a Components index (list of components responsibles), enrich it 
with links to the most recent component version and download link obtained
from a list of releases
"""
def enrich_components_list(components_index: list, releases:list, default_name_prefix:str = "") -> None:
    for component in components_index:
        name = component["Name"]
        prefix = default_name_prefix
        if "Prefix" in component:
            prefix = component["Prefix"]
        latest_release = None
        title = _get_component_title(prefix, component["Name"], component["Category"] )

        # Find the latest version in the releases list
        component_releases = list(filter(lambda r: r["title"]==title, releases))
        if(len(component_releases)> 0):
            if not _is_third_party_component(component):
                try: 
                    latest_release = max(component_releases, key=lambda r: parse_version(r["version"]))
                    component["Latest"] = latest_release["version"]
                    logging.debug(f"Latest release for component {name} : {latest_release['version']}")
                    component["zipDownloadUrl"] = _extract_url("ZIP",latest_release["softwareLinks"])
                    # If a URL is already specified, use it, otherwise, use the canonical component name+".html"
                    if "URL" not in component:
                      component["URL"] = get_canonical_name(title)+".html"
                      logging.debug(f"Setting {title} page URL to {component['URL']}")
                    else:
                      logging.info(f"Not overwriting preset URL for {title} and keeping : {component['URL']}")
                except:
                    logging.warning(f"Could not determine latest version for component {name}")
                    traceback.print_exc()
            else:
                logging.info(f"Not setting Latest Version and URLs for third party compomnent {title}")
        else:
            logging.debug(f"No release was found for component {name}.")

"""
Given a Components YAML index (list of components responsibles), produce a new 
dictionary associating the canonical component name to component releases list per component, listing all the known versions of the component.
"""
def generate_component_versions_files(components_index: list, releases:list, default_title_prefix:str = "") -> Dict[str,list]:
    result = {}
    for component in components_index:
        name :str = component["Name"]
        prefix : str = default_title_prefix
        if("Prefix" in component):
            prefix = component["Prefix"]

        title = _get_component_title(prefix, component["Name"], component["Category"] )
        
        if not _is_third_party_component(component):
            logging.debug(f"Searching for {title}")
            component_releases = list(filter(lambda r: r["title"]==title, releases))
            logging.debug(f"Found for {len(component_releases)} releases")
            # Add a convenient ZIP download URL in the release object
            for r in component_releases:
                r["zipDownloadUrl"] = _extract_url("ZIP", r["softwareLinks"])
            canonical_name = get_canonical_name(name)
            result[canonical_name] = component_releases
        else:
           logging.info(f"Not searching for releases for third-party component {title}")
    return result

"""
Given a Components YAML index (list of components responsibles), produce a new 
dictionary associating the component category to component releases, listing all the known versions of all matching components.
"""
def generate_categories_versions_files(components_index: list, releases:list, default_title_prefix:str = "") -> Dict[str,list]:
    result = {}
    category_to_component = {}
    for component in components_index:
        category_to_component.setdefault(component['Category'], []).append(component)
    for category in category_to_component:
        for component in category_to_component[category]:
            name :str = component["Name"]
            prefix : str = default_title_prefix
            if("Prefix" in component):
                prefix = component["Prefix"]

            title = _get_component_title(prefix, component["Name"], component["Category"] )
            
            if not _is_third_party_component(component):
                logging.debug(f"Searching for {title}")
                component_releases = list(filter(lambda r: r["title"]==title, releases))
                logging.debug(f"Found for {len(component_releases)} releases")
                # Add a convenient ZIP download URL in the release object
                for r in component_releases:
                    r["zipDownloadUrl"] = _extract_url("ZIP", r["softwareLinks"])
                canonical_name = get_canonical_name(name)
                result.setdefault(component['Category'], []).extend(component_releases)
            else:
                logging.info(f"Not searching for releases for third-party component {title}")
    return result



"""
Given an output path, write the given release objects into a YAML file
"""
def write_component_releases_file(component_name: str, output_dir: str, releases:list) -> None:
    if os.access(output_dir, os.W_OK):
        with open(output_dir+"/component-"+component_name+".yaml","w") as f:
            yaml.dump(releases,f)
    else:
        logging.warning(f"Cannot write component releases to location {output_dir}")
    

"""
Take the given component or category name and simplify it to serve as a filename
 (remove and trim blank characters, lowercase, replace spaces by dashes)
"""
def get_canonical_name(name : str) -> str:
    result: str = name
    result = result.strip().lower()
    result = result.replace(" ", "-")
    result = result.replace("(", "").replace(")", "")
    result = result.replace(".", "-")
    return result


"""
Denormalize column - convert a single column containing a list of elements into
multiple columns containing a boolean
"""
def denormalize_property(releases:list,attribute_name:str, value_if_present="Y", value_if_absent="N", possible_values:list = None) -> list:
    # First pass : identify all possible properties if not provided by possible_values
    if possible_values is None:
        possible_values = []
        for rel in releases:
            if attribute_name in rel:
                for value in rel[attribute_name]:
                    if value not in possible_values:
                        possible_values.append(value)
    
    # Second pass : create new attributes concatenating the attribute name and a boolean
    #  if the release feature that value in release[attribute_name]
    for rel in releases:
        for val in possible_values:
            new_property_name = attribute_name+"_"+get_canonical_name(val)
            if attribute_name in rel and (val in rel[attribute_name]):
                rel[new_property_name] = value_if_present
            else:
                rel[new_property_name] = value_if_absent
    
    return releases



def _get_component_title(name_prefix:str, name: str, category: str) -> str:
    title = name_prefix+name
    if(category == "Framework"):
        title = name
    return title

"""
Indicate if the given component entry is a third-party component (managed by an experiment, third-party contractor etc...)
"""
def _is_third_party_component(component) -> bool:
    if "ThirdParty" in component:
        return bool(component["ThirdParty"])
    return False


"""
Given an ARES formatted string of download links, extract the URL matching the given file type.
e.g. ZIP||https://edms.cern.ch/file/1057752/5.1.2/fwAccessControl-5.1.2.zip
"""
def _extract_url(type: str, links: list)-> str:
    result = None
    for link in links:
        try:
          if link.startswith(type):
            result = link.split("||")[1]
        except:
          logging.warning(f"Could not extract {type} link from {link}")
    return result


