import os
import logging
import sys
from urllib.request import urlopen
import traceback
from dotenv import load_dotenv
load_dotenv()
import yaml
import json
import socket

from .utils import  *

import argparse
import logging

parser = argparse.ArgumentParser()
parser.add_argument(
    '-d', '--debug',
    help="Print lots of debugging statements",
    action="store_const", dest="loglevel", const=logging.DEBUG,
    default=logging.WARNING,
)
parser.add_argument(
    '-v', '--verbose',
    help="Be verbose",
    action="store_const", dest="loglevel", const=logging.INFO,
)
args = parser.parse_args()    
logging.basicConfig(level=args.loglevel)


yaml_components_list = None
yaml_components_output_file = None
json_releases = None
output_dir = None

yaml_components_output_prefix = os.environ.get("ARES_YAML_COMPONENTS_OUTPUT_FILE_PREFIX", "components")
yaml_components_output_file = yaml_components_output_prefix+".yaml"
# Assert that OUTPUT DIR is writeable
output_dir = os.environ.get("ARES_COMPONENTS_OUTPUT_DIR")
if(output_dir is None):
    logging.error("Please provide an output directory via COMPONENTS_OUTPUT_DIR")
    sys.exit(1)

output_dir_exists = os.path.exists(output_dir)
if not output_dir_exists:
    os.makedirs(output_dir)

if not os.access(output_dir, os.W_OK):
    logging.error(f"Output directory {output_dir} is not writable.")
    sys.exit(1)

# Collect COMPONENTS LIST and invoke utility functions
url = os.environ.get("ARES_YAML_COMPONENTS_LIST_URL")
try:
    with urlopen(url,timeout=10) as response:
        yaml_components_list = yaml.safe_load(response.read())
except socket.timeout as e:
    logging.error(f"Timeout while requesting Components list URL {url}")
    sys.exit(1)
except:
    logging.error("Unexpected error while parsing Components List : "+ sys.exc_info()[0])
    sys.exit(1)

# Collect RELEASES JSON FILE and invoke utility functions
url = os.environ.get("ARES_JSON_RELEASES_URL")
try:
    with urlopen(os.environ.get("ARES_JSON_RELEASES_URL"),timeout=10) as response:
        json_releases = json.load(response)
except socket.timeout as e:
    logging.error(f"Timeout while requesting JSON releases URL {url}")
    sys.exit(1)
except:
    logging.error("Unexpected error while parsing Releases file : "+ sys.exc_info()[0])
    sys.exit(1)

# In the JSON release file, component names are prefix with the framework name :
# e.g. "JCOP Framework 3D Viewer" - while the short component name is simply "3D Viewer"
# Note that component can specify their own Prefix individually
ARES_COMPONENT_DEFAULT_NAME_PREFIX: str = os.environ.get("ARES_COMPONENT_DEFAULT_NAME_PREFIX", "")

json_releases = cleanup_mongodb_data(json_releases)
json_releases = denormalize_property(json_releases,"platforms", "&#10004;","&#10060;")

#####################3
# Enrich the Components list
enrich_components_list(yaml_components_list, json_releases, ARES_COMPONENT_DEFAULT_NAME_PREFIX)
# ... and write it to the local file system
try:
    out_file = output_dir+"/"+yaml_components_output_file
    logging.info(f"Writing components list file {out_file}")
    with open(out_file, "w") as outfile:
        yaml.dump(yaml_components_list, outfile)
        logging.debug(f"Done writing components list file {out_file}")
except:
    logging.warning(f"Could not write enriched components list in {output_dir}")
    traceback.print_exc()
# ... and write one component file per category
category_to_component = {}
for component in yaml_components_list:
    category_to_component.setdefault(component['Category'], []).append(component)

for category in category_to_component:
    try:
        out_file = output_dir+"/"+yaml_components_output_prefix+"-"+get_canonical_name(category)+".yaml"
        logging.info(f"Writing components category list file {out_file}")
        with open(out_file, "w") as outfile:
            yaml.dump(category_to_component[category], outfile)
            logging.debug(f"Done writing components list file {out_file}")
    except:
        logging.warning(f"Could not write components category list in {output_dir}")
        traceback.print_exc()
#
#######

#######
# Split the releases file into multiple files
components_dict = generate_component_versions_files(yaml_components_list,json_releases, ARES_COMPONENT_DEFAULT_NAME_PREFIX)

for component_name in components_dict:
    releases_count = len(components_dict[component_name])
    if(releases_count>0):
        logging.info(f"Generating {component_name}  ({ARES_COMPONENT_DEFAULT_NAME_PREFIX}) with {releases_count} releases.")
        try:
            write_component_releases_file(component_name,output_dir,components_dict[component_name])
            logging.debug(f"Done generating {component_name}  ({ARES_COMPONENT_DEFAULT_NAME_PREFIX}) releases.")
        except:
            logging.warning(f"Could not write components release file for {component_name} in {output_dir}")
            traceback.print_exc()
    else:
        logging.warning(f"Component {component_name} has no known release - skipped.")
#######

#######
# Split the releases file into a single file per category
categories_dict = generate_categories_versions_files(yaml_components_list,json_releases, ARES_COMPONENT_DEFAULT_NAME_PREFIX)

for category in categories_dict:
    releases_count = len(categories_dict[category])
    if(releases_count>0):
        logging.info(f"Generating Category {category} file ({ARES_COMPONENT_DEFAULT_NAME_PREFIX}) with {releases_count} releases.")
        try:
            if os.access(output_dir, os.W_OK):
                with open(output_dir+"/category-"+get_canonical_name(category)+".yaml","w") as f:
                    yaml.dump(categories_dict[category],f)
            else:
                logging.warning(f"Cannot write Category releases file to location {output_dir}")
            logging.debug(f"Done generating Category {category} file ({ARES_COMPONENT_DEFAULT_NAME_PREFIX}) releases.")
        except:
            logging.warning(f"Could not write Category release file for {category} in {output_dir}")
            traceback.print_exc()
    else:
        logging.warning(f"Category {category} has no known release - skipped.")
#######


