from setuptools import setup

setup(
   name='ares-utils',
   version='0.1',
   description='A library to perform data transformations in ARES',
   author='Brice Copy',
   author_email='brice.copy@cern.ch',
   packages=['ares'],
   install_requires=['pyyaml', 'requests', 'python-dotenv'],
   url="https://gitlab.cern.ch/industrial-controls/sw-infra/ares/ares-utils",
   classifiers=[
      "Programming Language :: Python :: 3",
      "License :: CERN Copyright",
      "Operating System :: OS Independent",
   ],
   python_requires='>=3.6',
)
