# ARES Python Utilities

Various utility classes for ARES in Python (data pre-processing)

## Vocabulary

The module combines various data sources :

* YAML COMPONENTS LIST : The URL to a list of framework components, in YAML format, that provides the existing components, their responsible, CERN organic unit.
* JSON RELEASES FILE : The URL to a JSON list of components releases. A releases file is typically produced by the ARES toolset and contains information about all known framework components.


## How to use the module

You can install the module directly via git (for instance v1.0.1):
```
pip install git+https://gitlab.cern.ch/industrial-controls/sw-infra/ares/ares-utils.git@v1.0.1
```

You can provide arguments to the module via environment variables or a ``.env`` file containing the following :

```bash
ARES_YAML_COMPONENTS_LIST_URL=file://path/to/components.yaml
ARES_YAML_COMPONENTS_OUTPUT_FILE=components.yaml
ARES_JSON_RELEASES_URL=https://cern.ch/jcop/releases.json
ARES_COMPONENTS_OUTPUT_DIR=path/to/output/path
# (!) Note the trailing space at the end of the prefix
ARES_COMPONENT_DEFAULT_NAME_PREFIX="JCOP Framework "
```

Finally, invoke the module :
```bash
python -m ares
```

This will generate the file in your ``$COMPONENTS_OUTPUT_DIR`` folder.

## How to run integration tests

Source the desired environment and invoke the module :
```bash
export $(xargs < integration-test/SIMPLE.env)
python -m ares
```